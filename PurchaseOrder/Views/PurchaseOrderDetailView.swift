//
//  PurchaseOrderDetailView.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import SwiftUI

struct PurchaseOrderDetailView: View {
    
    @State var purchaseOrder: PurchaseOrder
    let colors: [Int: Color] = [1: .green, 0: .red]
    
    var body: some View {
        List {
            Section(header: Text("Items")) {
                ForEach(purchaseOrder.items) {item in
                    HStack {
                        VStack(alignment: .leading, spacing: 10, content: {
                            Text("ID: \(item.id ?? 1)")
                            Text("Quantity: \(item.quantity ?? 1)")
                        })
                        
                        Spacer()
                        
                        Text("\(purchaseOrder.last_updated?.getShortDate() ?? Date().getShortDate())")
                    }
                }
            }
            
            Section(header: Text("Invoices")) {
                ForEach(purchaseOrder.invoices) {invoice in
                    HStack {
                        VStack(alignment: .leading, spacing: 10, content: {
                            Text("ID: \(invoice.id ?? 1)")
                            Text("Invoice No: \(invoice.invoice_number ?? "")")
                            
                            HStack {
                                Text("Status:")
                                
                                Text("\(invoice.received_status ?? 1)")
                                    .font(.caption)
                                    .fontWeight(.black)
                                    .padding(5)
                                    .background(colors[invoice.received_status ?? 1, default: .black])
                                    .foregroundColor(Color.white)
                                    .clipShape(Circle())
                            }
                        })
                        
                        Spacer()
                        
                        Text("\(purchaseOrder.last_updated?.getShortDate() ?? Date().getShortDate())")
                    }
                }
            }
        }
        .listStyle(GroupedListStyle())
        .navigationTitle("ID: \(purchaseOrder.id ?? 1)")
        .navigationBarItems(trailing:
            Button(action: {
                print("User icon pressed...")
            }) {
                Image(systemName: "plus.circle.fill").imageScale(.large)
            }
        )
    }
    
}

struct PurchaseOrderDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PurchaseOrderDetailView(purchaseOrder: PurchaseOrder.getSampleItem())
    }
}
