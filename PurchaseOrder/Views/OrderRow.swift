//
//  OrderRow.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import SwiftUI

struct OrderRow: View {
    
    var purchaseOrder: PurchaseOrder
    
    var body: some View {
        HStack {
            VStack {
                Text("PO ID: \(purchaseOrder.id ?? 1)")
                Text("Items: \(purchaseOrder.items.count)")
            }
            
            Spacer()
            Text("\(purchaseOrder.last_updated?.getShortDate() ?? Date().getShortDate())")
        }
    }
}

struct OrderRow_Previews: PreviewProvider {
    static var previews: some View {
        let item = PurchaseOrder.getSampleData().first!
        OrderRow(purchaseOrder: item)
    }
}
