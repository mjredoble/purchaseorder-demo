//
//  ContentView.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import SwiftUI

struct PurchaseOrderView: View {
    
    @State var purchaseOrders: [PurchaseOrder] = []
    
    var body: some View {
        NavigationView {
            List {
                ForEach(purchaseOrders) { item in
                    NavigationLink(destination: PurchaseOrderDetailView(purchaseOrder: item)) {
                        OrderRow(purchaseOrder: item)
                    }
                }
            }
            .navigationTitle("Purchase Orders")
            .navigationBarItems(trailing:
                Button(action: {
                    print("User icon pressed...")
                }) {
                    Image(systemName: "plus.circle.fill").imageScale(.large)
                }
            )
        }
        .onAppear() {
            ApiService().getPurchaseOrders(completionHandler: { results in
                self.purchaseOrders = results
            })
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        PurchaseOrderView(purchaseOrders: PurchaseOrder.getSampleData())
    }
}
