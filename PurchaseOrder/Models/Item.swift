//
//  Item.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import Foundation

final class Item: Codable, Identifiable {
    var id: Int?
    var product_item_id: Int?
    var quantity: Int?
    var last_updated_user_entity_id: Int?
    var transient_identifier: String?
    var active_flag: Bool?
    var last_updated: Date?
}
