//
//  Cancellation.swift
//  PurchaseOrder
//
//  Created by Michael Joseph Redoble on 13/06/2021.
//

import Foundation

/*
 {
   "id": 1,
   "product_item_id": 2,
   "ordered_quantity": 11,
   "last_updated_user_entity_id": 0,
   "created": "2020-05-07T09:32:28.213Z",
   "transient_identifier": "string"
 }
 */

class Cancellation: Codable, Identifiable {
    var id: Int?
    var product_item_id: Int?
    var ordered_quantity: Int?
    var last_updated_user_entity_id: Int?
    var created: String?
    var transient_identifier: String?
}
